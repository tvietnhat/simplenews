//
//  ArticleViewController.h
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Article;

@interface ArticleViewController : UIViewController

@property (nonatomic, retain) Article *article;

@end
