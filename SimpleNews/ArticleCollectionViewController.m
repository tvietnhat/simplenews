//
//  ArticleCollectionViewController.m
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "ArticleCollectionViewController.h"

#import "ArticleCollectionViewController.h"
#import "ArticleCollectionLoadOperation.h"
#import "ArticleCollection.h"
#import "Article.h"
#import "ActivityIndicatorCell.h"
#import "ArticleCell.h"
#import "ArticleViewController.h"
#import "Functions.h"


@interface ArticleCollectionViewController ()

@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property (nonatomic, assign) BOOL isLoadingArticleCollection;
@property (nonatomic, retain) ArticleCollection *articleCollection;

@end


@implementation ArticleCollectionViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationItem.title = @"News";
    
    // create the operation queue to handle background operations
    if (!self.operationQueue) {
        self.operationQueue = [[[NSOperationQueue alloc] init] autorelease];
    }
    
    // register table cell classes for UITableView
    [self.tableView registerClass:[ArticleCell class] forCellReuseIdentifier:@"ArticleCell"];
    [self.tableView registerClass:[ActivityIndicatorCell class] forCellReuseIdentifier:@"ActivityIndicatorCell"];
    
    // create refresh control to enable Pull To Refresh gesture
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.attributedTitle = [[[NSAttributedString alloc] initWithString:@"Refresh Articles"] autorelease];
    [refreshControl addTarget:self action:@selector(refreshArticleCollection) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    [refreshControl release];
    
    // create refresh button
    UIBarButtonItem *refreshButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Refresh" style:UIBarButtonItemStyleBordered target:self action:@selector(refreshButtonTapped:)];
    self.navigationItem.leftBarButtonItem = refreshButtonItem;

    // start loading the articles
    [self loadArticleCollection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    for (NSOperation *operation in _operationQueue.operations) {
        [operation removeObserver:self forKeyPath:@"isFinished"];
    }
    [_operationQueue release];
    
    [_articleCollection release];
    
    [super dealloc];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (self.isLoadingArticleCollection) {
        return 1;
    }
    else {
        return [self.articleCollection.items count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isLoadingArticleCollection) {
        NSString *CellIdentifier = @"ActivityIndicatorCell";
        ActivityIndicatorCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        [cell.activityIndicator startAnimating];
        
        return cell;
        
    } else {
        NSString *CellIdentifier = @"ArticleCell";
        ArticleCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        Article *article = self.articleCollection.items[indexPath.row];
        cell.article = article;
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight;
    
    if (self.isLoadingArticleCollection) {
        rowHeight = 100;
    } else {
        Article *article = self.articleCollection.items[indexPath.row];
        rowHeight = [ArticleCell tableView:tableView rowHeightForArticle:article accessoryViewWidth:0];
    }
    
    return rowHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (!self.isLoadingArticleCollection) {
        // show the detail view controller for selected article
        Article *article = self.articleCollection.items[indexPath.row];
        ArticleViewController *viewController = [[ArticleViewController alloc] init];
        viewController.article = article;
        [self.navigationController pushViewController:viewController animated:YES];
        [viewController release];
    }
    
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */


#pragma mark - other methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([object isKindOfClass:[ArticleCollectionLoadOperation class]] && [keyPath isEqualToString:@"isFinished"]) {
        
        // simulate loading time
        [NSThread sleepForTimeInterval:2];
        
        // notify view controler that the operation is finished. Do this on main thead as it involves UI update
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadArticleCollectionFinished:object];
        });
    }
}

- (void)loadArticleCollection
{
    if (!self.isLoadingArticleCollection) {
        self.isLoadingArticleCollection = YES;
        
        // create the operation, set the observer and add it to the operation queue
        ArticleCollectionLoadOperation *operation = [[ArticleCollectionLoadOperation alloc] init];
        [operation addObserver:self forKeyPath:@"isFinished" options:0 context:NULL];
        [self.operationQueue addOperation:operation];
        [operation release];
    }
}

- (void) loadArticleCollectionFinished:(ArticleCollectionLoadOperation *)operation
{
    self.isLoadingArticleCollection = NO;
    
    if (operation.error) {
        // handle error if application failed to load articles
        UIAlertView *alertView = [[[UIAlertView alloc] initWithTitle:@"Error" message:@"An error has occurred while loading articles" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] autorelease];
        [alertView show];
    }
    else {
        // otherwise display the data
        self.articleCollection = operation.articleCollection;
        self.navigationItem.title = self.articleCollection.name;
        [self.tableView reloadData];
    }
    
    // stop the refresh control animation
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
}

- (void) refreshArticleCollection
{
    [self loadArticleCollection];
}


- (void) refreshButtonTapped:(id)sender
{
    [self loadArticleCollection];
    [self.tableView reloadData];
}

@end
