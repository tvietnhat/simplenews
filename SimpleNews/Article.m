//
//  Article.m
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "Article.h"
#import "Functions.h"


NSString * const kArticleDateTimeFormat = @"yyyy-MM-dd'T'HH:mm:sszzz";


@implementation Article

@synthesize webHref = _webHref;
@synthesize identifier = _identifier;
@synthesize headLine = _headLine;
@synthesize slugLine = _slugLine;
@synthesize dateLine = _dateLine;
@synthesize tinyUrl = _tinyUrl;
@synthesize type = _type;
@synthesize thumbnailImageHref = _thumbnailImageHref;

- (void)dealloc
{
    [_webHref release];
    [_identifier release];
    [_headLine release];
    [_slugLine release];
    [_dateLine release];
    [_tinyUrl release];
    [_type release];
    [_thumbnailImageHref release];
    
    [super dealloc];
}

- (void) loadFromDictionary:(NSDictionary *)dictionary
{
    self.webHref = isNSNull(dictionary[@"webHref"], nil);
    self.identifier = isNSNull(dictionary[@"identifier"], nil);
    self.headLine = isNSNull(dictionary[@"headLine"], nil);
    self.slugLine = isNSNull(dictionary[@"slugLine"], nil);
    
    NSString *dateLine = isNSNull(dictionary[@"dateLine"], nil);
    self.dateLine = [[self dateFormatter] dateFromString:dateLine];
    
    self.tinyUrl = isNSNull(dictionary[@"tinyUrl"], nil);
    self.type = isNSNull(dictionary[@"type"], nil);
    self.thumbnailImageHref = isNSNull(dictionary[@"thumbnailImageHref"], nil);
}

- (NSDateFormatter *) dateFormatter
{
    static NSDateFormatter * _dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = kArticleDateTimeFormat;
    });
    
    return _dateFormatter;
}

@end
