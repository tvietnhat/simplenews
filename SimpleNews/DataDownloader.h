//
//  DataDownloader.h
//  
//
//  Created by Nhat Tran on 24/07/10.
//  Copyright 2010 Rubik Consulting. All rights reserved.
//

#import <Foundation/Foundation.h>



@class DataCacheEngine;
@protocol DataDownloaderDelegate;


@interface DataDownloader : NSObject  {
}

@property (nonatomic, assign) id <DataDownloaderDelegate> delegate;

@property (nonatomic, readonly) DataCacheEngine *cacheEngine;
@property (nonatomic, retain) NSMutableData *activeDownload;
@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, retain) NSURLRequest *request;
@property (nonatomic, readonly) NSData *data;
@property (nonatomic, readonly) NSError *error;
@property (nonatomic, readonly) BOOL isFinished;
@property (nonatomic, readonly) BOOL isSuccessful;
@property (nonatomic, assign) BOOL shouldAcceptUntrustedServer;
@property (nonatomic, assign) BOOL cacheEnabled;
@property (nonatomic, assign) BOOL tryToDownloadBeforeUsingCache;
@property (nonatomic, readonly) BOOL isCacheAvailable;
@property (nonatomic, retain) id context;
@property (nonatomic, readonly) NSString *url;

- (id) initWithRequest:(NSURLRequest *)aRequest;
- (id) initWithUrl:(NSString *)url;

- (void) startDownload;
- (void) cancelDownload;

- (void) downloaderDidFinish;
- (void) downloaderDidFail;

- (NSTimeInterval) cacheExpiration;
- (NSString *) cacheCategory;
- (BOOL) validateDownloadedData;

@end


@protocol DataDownloaderDelegate <NSObject>

- (void) dataDownloaderDidFinish:(DataDownloader *)downloader;

@optional
- (void) dataDownloaderDidFail:(DataDownloader *)downloader withError:(NSError *)error;

@end

