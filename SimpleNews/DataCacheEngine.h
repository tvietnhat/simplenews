//
//  DataCacheEngine.h
//  
//
//  Created by Nhat Tran on 1/02/11.
//  Copyright 2011 . All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSTimeInterval const kDefaultCacheExpiration;
extern NSString * const kDefaultCacheCategory;


@interface DataCacheEngine : NSObject

@property (nonatomic, retain) NSString *cacheCategory;

- (NSString *) cacheFilePathForKey:(NSString *)key;

- (NSData *) dataForKey:(NSString *)key error:(NSError **)error;
- (void) setData:(NSData *)data forKey:(NSString *)key;

- (id) objectForKey:(NSString *)key;
- (void) setObject:(id)object forKey:(NSString *)key;

- (BOOL) isCacheValidForKey:(NSString *)key cacheExpiration:(NSTimeInterval)cacheExpiration;


@end
