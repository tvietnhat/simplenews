//
//  ArticleCollectionLoadOperation.m
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "ArticleCollectionLoadOperation.h"

#import "ArticleCollectionLoadOperation.h"
#import "ArticleCollection.h"
#import "ArticleDataAdapter.h"


@implementation ArticleCollectionLoadOperation

@synthesize articleCollection = _articleCollection;
@synthesize error = _error;

- (void)dealloc
{
    [_articleCollection release];
    [_error release];
    
    [super dealloc];
}


- (void)main
{
    NSError *error = nil;
    
    ArticleDataAdapter *dataAdapter = [[[ArticleDataAdapter alloc] init] autorelease];;
    
    self.articleCollection = [dataAdapter getArticleCollection:&error];
    
    self.error = error;
}

@end
