//
//  ArticleCell.h
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Article;
@class UILazyImageView;

@interface ArticleCell : UITableViewCell {
    UILabel *labelHeadline;
    UILabel *labelSlugLine;
    UILazyImageView *thumbnailView;
}

@property (nonatomic, retain) Article *article;

+ (CGFloat) tableView:(UITableView *)tableView rowHeightForArticle:(Article *)article accessoryViewWidth:(CGFloat)accessoryViewWidth;

@end
