//
//  Functions.m
//  
//
//  Created by Nhat Tran on 13/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "Functions.h"


#pragma mark - common functions

id isNil(id obj, id replacementObj)
{
    assert(replacementObj);
    return obj ? obj : replacementObj;
}


id isNSNull(id obj, id replacementObj)
{
    assert(obj);
    return [obj isKindOfClass:[NSNull class]] ? replacementObj : obj;
}

