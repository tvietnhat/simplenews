//
//  DataDownloader.m
//
//
//  Created by Nhat Tran on 24/07/10.
//  Copyright 2010 Rubik Consulting. All rights reserved.
//

#import "DataDownloader.h"
#import "DataCacheEngine.h"




@interface DataDownloader ()

- (void) setError:(NSError *)anError;


@end

@implementation DataDownloader

@synthesize delegate = _delegate;
@synthesize cacheEngine = _cacheEngine;
@synthesize activeDownload = _activeDownload;
@synthesize connection = _connection;
@synthesize request = _request;
@synthesize isFinished = _isFinished;
@synthesize isSuccessful = _isSuccessful;
@synthesize shouldAcceptUntrustedServer = _shouldAcceptUntrustedServer;
@synthesize cacheEnabled = _cacheEnabled;
@synthesize tryToDownloadBeforeUsingCache = _tryToDownloadBeforeUsingCache;
@synthesize error = _error;
@synthesize context = _context;



- (id) initWithRequest:(NSURLRequest *)aRequest {
    self = [super init];
	if (self) {
		_cacheEngine = [[DataCacheEngine alloc] init];
		_cacheEngine.cacheCategory = [self cacheCategory];
		_request = aRequest;
	}
	return self;
}


- (id) initWithUrl:(NSString *)url {
    self = [super init];
	if (self) {
		_cacheEngine = [[DataCacheEngine alloc] init];
		_cacheEngine.cacheCategory = [self cacheCategory];
		_request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
	}
	return self;
}


- (NSTimeInterval) cacheExpiration {
	return kDefaultCacheExpiration;
}

- (NSString *) cacheCategory {
	return kDefaultCacheCategory;
}


- (NSString *) url {
	return [[self.request URL] absoluteString];
}

- (void) setError:(NSError *)anError {
	if (_error != anError) {
        _error = anError;
	}
}

- (NSData *) data {
	return self.activeDownload;
}

-  (BOOL)isCacheAvailable {
    return [self.cacheEngine isCacheValidForKey:[[self.request URL] absoluteString] cacheExpiration:self.cacheExpiration];
}

- (void) startDownload {
	_isSuccessful = NO;
	_isFinished = NO;

	if (self.cacheEnabled) {
		if (self.isCacheAvailable && !self.tryToDownloadBeforeUsingCache) {
            dispatch_async(dispatch_queue_create("LoadFromCacheQueue", NULL), ^{
                [self loadDataFromCache];
            });
		} else {
			NSLog(@"Start downloading data at %@", self.url);
			self.activeDownload = [NSMutableData data];
			self.connection = [NSURLConnection connectionWithRequest:self.request delegate:self];
		}
	} else {
		self.activeDownload = [NSMutableData data];
		self.connection = [NSURLConnection connectionWithRequest:self.request delegate:self];
	}

}


- (void) cancelDownload
{
    [self.connection cancel];
    self.connection = nil;
    self.activeDownload = nil;
	_isFinished = YES;
	_isSuccessful = NO;
}

- (void) loadDataFromCache {
	@autoreleasepool {
	
		NSError *anError = nil;
		NSString *filePath = [self.cacheEngine cacheFilePathForKey:[[self.request URL] absoluteString]];
		self.activeDownload = [NSMutableData dataWithContentsOfFile:filePath options:0 error:&anError];
		if (anError) {
			[self setError:anError];
			[self downloaderDidFail];
		} else {
			if ([self validateDownloadedData]) {
				[self downloaderDidFinish];
			} else {
				[self downloaderDidFail];
			}
		}
	
	}
}


- (void) downloaderDidFinish {
	_isSuccessful = YES;
	_isFinished = YES;
    if ([self.delegate respondsToSelector:@selector(dataDownloaderDidFinish:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate dataDownloaderDidFinish:self];
        });
    }
}

- (void) downloaderDidFail {
	NSLog(@"Error occurred when downloading data: %@", self.error);

	_isSuccessful = NO;
	_isFinished = YES;
	if ([self.delegate respondsToSelector:@selector(dataDownloaderDidFail:withError:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate dataDownloaderDidFail:self withError:self.error];
        });
	}
}

- (BOOL) validateDownloadedData {
	return YES;
}


#pragma mark - Download support (NSURLConnectionDelegate)

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.activeDownload appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)anError
{
	// Clear the activeDownload property to allow later attempts
    self.activeDownload = nil;
    
    // Release the connection now that it's finished
    self.connection = nil;
	
	[self setError:anError];
	
    if (self.cacheEnabled && self.tryToDownloadBeforeUsingCache) {
        [self loadDataFromCache];
    } else {
        [self downloaderDidFail];
    }
	self.request = nil;
}


- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{	
    // clean up after finished
    self.connection = nil;
	
	if ([self validateDownloadedData]) {
		[self.cacheEngine setData:self.activeDownload forKey:[[self.request URL] absoluteString]];
		[self downloaderDidFinish];
	} else {
        if (self.cacheEnabled && self.tryToDownloadBeforeUsingCache) {
            [self loadDataFromCache];
        } else {
            [self downloaderDidFail];
        }
	}
	
    self.activeDownload = nil;
	self.request = nil;
}


- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    assert( [response isKindOfClass:[NSHTTPURLResponse class]] );
	
	if (httpResponse.statusCode / 100 != 2) {	// HTTP Error Occurred
		NSError *anError = [NSError errorWithDomain:[NSString stringWithFormat:@"HTTP %d Error. %@", httpResponse.statusCode, self.request.URL.absoluteString] code:httpResponse.statusCode userInfo:nil];
		[self setError:anError];
		[self downloaderDidFail];
	}
}


- (BOOL) connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	NSLog(@"canAuthenticateAgainstProtectionSpace");
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void) connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	NSLog(@"didCancelAuthenticationChallenge");
}


- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	NSLog(@"didReceiveAuthenticationChallenge");
	
	if (self.shouldAcceptUntrustedServer) {
		if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
			//if ([trustedHosts containsObject:challenge.protectionSpace.host]) {
			[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
			//}
		}
		
		[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
	} else {
		[challenge.sender useCredential:challenge.proposedCredential forAuthenticationChallenge:challenge];
	}
}


@end
