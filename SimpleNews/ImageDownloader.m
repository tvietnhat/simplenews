//
//  ImageDownloader.m
//  BargainEveryday
//
//  Created by Nhat Tran on 21/09/10.
//  Copyright 2010 Rubik Consulting. All rights reserved.
//

#import "ImageDownloader.h"


@implementation ImageDownloader

@synthesize image = _image;

- (id) initWithUrl:(NSString *)url {
	if (self = [super initWithUrl:url]) {
		self.cacheEnabled = YES;
	}
	
	return self;
}

- (void)dealloc
{
    [_image release];
    
    [super dealloc];
}

- (BOOL) validateDownloadedData {
	self.image = [UIImage imageWithData:self.activeDownload];
	return self.image != nil;
}


@end
