//
//  ArticleCollection.m
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//


#import "ArticleCollection.h"
#import "Functions.h"
#import "Article.h"


@implementation ArticleCollection

@synthesize identifier = _identifier;
@synthesize name = _name;
@synthesize items = _items;


- (void)dealloc
{
    [_identifier release];
    [_name release];
    [_items release];
    
    [super dealloc];
}


- (void) loadFromDictionary:(NSDictionary *)dictionary
{
    self.identifier = isNSNull(dictionary[@"identifier"], nil);
    self.name = isNSNull(dictionary[@"name"], nil);
    
    
    // load article list
    NSArray *itemJSONArray = isNSNull(dictionary[@"items"], nil);
    
    if (itemJSONArray) {
        NSMutableArray *itemArray = [NSMutableArray arrayWithCapacity:[itemJSONArray count]];
        
        for (NSDictionary *itemDict in itemJSONArray) {
            Article *article = [[Article alloc] init];
            [article loadFromDictionary:itemDict];
            [itemArray addObject:article];
            [article release];
        }
        
        self.items = itemArray;
    }
    else {
        self.items = nil;
    }
    
}


@end
