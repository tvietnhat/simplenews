//
//  DataCacheEngine.m
//  
//
//  Created by Nhat Tran on 1/02/11.
//  Copyright 2011 . All rights reserved.
//

#import "DataCacheEngine.h"
#import "CryptoProvider.h"
#import "Base64.h"
#import "NSString+Extend.h"


NSTimeInterval const kDefaultCacheExpiration = (3600 * 24 * 30); // 30 days
NSString * const kDefaultCacheCategory = @"shared";
NSString * const kCacheRootDirectory = @"URLCache";
NSString * const kSanitizedCharacters = @":/*\\";
NSString * const kReplacedString = @"_"; // must be a 1-character string

static NSString *cacheRootPath;


@interface DataCacheEngine (Private)
- (NSString *) cacheCategoryPath;
- (NSDate *) modificationDateOfFile:(NSString *)path;

@end


@implementation DataCacheEngine

@synthesize cacheCategory = _cacheCategory;

+ (void) initialize {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *rootDir = [[paths objectAtIndex:0] stringByAppendingPathComponent:kCacheRootDirectory];
	
	/* check for existence of cache directory */
	if (![[NSFileManager defaultManager] fileExistsAtPath:rootDir]) {
		/* create a new cache directory */
		NSError *error = nil;
		if (![[NSFileManager defaultManager] createDirectoryAtPath:rootDir 
									   withIntermediateDirectories:NO
														attributes:nil 
															 error:&error]) {
			NSLog(@"Error when creating Cache Root Directory: %@", error);
		}
	}
	cacheRootPath = [rootDir retain];
}


- (id) init {
	if (self = [super init]) {
		_cacheCategory = kDefaultCacheCategory;
	}
	return self;
}

- (void)dealloc
{
    [_cacheCategory release];
    
    [super dealloc];
}

- (NSString *) encodeKey:(NSString *)key {
    NSString *encodedKey = [key stringByEncodingURL];
    
    if (encodedKey.length >= 255) {
        NSString *hashedKey = [[CryptoProvider defaultProvider] getSHA1HashForString:key encoding:NSUTF8StringEncoding];
        NSLog(@"CacheEngine hashedKey %@", hashedKey);
        encodedKey = [hashedKey stringByEncodingURL];
    }
    
    return encodedKey;
}

- (NSString *) cacheCategoryPath {
	NSString *path = [cacheRootPath	stringByAppendingPathComponent:[self cacheCategory]];
	
	if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
		/* create a new cache directory */
		NSError *anError = nil;
		if (![[NSFileManager defaultManager] createDirectoryAtPath:path 
									   withIntermediateDirectories:NO
														attributes:nil 
															 error:&anError]) {
			NSLog(@"Error when creating Cache Category Directory: %@", anError);
		}
	}
	
	return path;
}

- (NSString *) cacheFilePathForKey:(NSString *)key {
    NSString *tmpFileName = [self encodeKey:key];
	
    NSString *path = [[self cacheCategoryPath] stringByAppendingPathComponent:tmpFileName];
	return path;
}


- (NSDate *) modificationDateOfFile:(NSString *)path {
	NSDate *date = nil;
	
	if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
		NSError *anError = nil;
		/* retrieve file attributes */
		NSDictionary *attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&anError];
		if (attributes != nil) {
			date = [attributes fileModificationDate];
		}
		else {
			NSLog(@"Error when getting file attribute: %@", anError);
		}
	}
	
	return date;
}

- (BOOL) isCacheValidForKey:(NSString *)key cacheExpiration:(NSTimeInterval)cacheExpiration {
	NSString *filePath = [self cacheFilePathForKey:key];
	NSDate *modificationDate = [self modificationDateOfFile:filePath];
	NSTimeInterval duration = [[NSDate date] timeIntervalSinceDate:modificationDate];
	return modificationDate != nil && duration < cacheExpiration;
}

- (void) setData:(NSData *)data forKey:(NSString *)key {
	NSError *anError = nil;
	NSString *filePath = [self cacheFilePathForKey:key];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	if ([fileManager fileExistsAtPath:filePath]) {
		if (![fileManager removeItemAtPath:filePath error:&anError]) {
			NSLog(@"Error occurred when deleting file: %@", anError);
		}
	}
	
	if (data && [fileManager fileExistsAtPath:filePath] == NO) {
		/* file doesn't exist, so create it */
		[fileManager createFileAtPath:filePath 
							 contents:data 
						   attributes:nil];
		
		//NSLog(@"newly cached image: %@", filePath);
	}
}

- (NSData *) dataForKey:(NSString *)key error:(NSError **)error {
	NSString *filePath = [self cacheFilePathForKey:key];
	return [NSData dataWithContentsOfFile:filePath options:0 error:error];
}

- (void) setObject:(id)object forKey:(NSString *)key {
    NSError *error = nil;
	NSData *data = [NSJSONSerialization dataWithJSONObject:object options:0 error:&error];
    if (error) {
        NSLog(@"Error occurred when serializing object to JSON: %@", error);
    }
	[self setData:data forKey:key];
}

- (id) objectForKey:(NSString *)key {
	NSError *error = nil;
	id obj = nil;
	
	NSData *data = [self dataForKey:key error:&error];
	if (!error && data) {
		obj = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
	}
	return obj;
}

@end
