//
//  AppDelegate.h
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (retain, nonatomic) UIWindow *window;

@end
