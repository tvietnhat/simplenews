//
//  UILazyImageView.h
//  
//
//  Created by Nhat Tran on 16/02/14.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILazyImageView : UIImageView

@property (nonatomic, retain) NSString *imageURL;

@end
