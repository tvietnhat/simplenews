//
//  NSString+Extend.h
//  
//
//  Created by Nhat Tran on 25/06/10.
//  Copyright 2010 Nhat Tran. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (Extend)

- (BOOL) isEmpty;
- (BOOL) containsCharacter:(unichar)character;
- (BOOL) startsWith:(NSString *)string options:(NSStringCompareOptions)mask;
- (BOOL) endsWith:(NSString *)string options:(NSStringCompareOptions)mask;

- (NSString *) stringByEncodingURL;
- (NSString *) stringByDecodingURL;

+ (NSString *) emptyString;
+ (BOOL) isNullOrEmpty:(NSString *)aString;

@end
