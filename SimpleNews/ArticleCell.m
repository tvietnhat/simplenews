//
//  ArticleCell.m
//  SimpleNews
//
//  Created by Nhat Tran on 24/09/2014.
//  Copyright (c) 2014 Nhat Tran. All rights reserved.
//

#import "ArticleCell.h"
#import "Article.h"
#import "UILazyImageView.h"
#import "NSString+Extend.h"
#import "UIView+Extend.h"
#import "ColorGradientView.h"


#define TITLE_FONE_SIZE 15
#define CONTENT_TEXT_FONT_SIZE 13
#define CONTENT_MARGIN 6
#define THUMB_IMAGE_WIDTH 100
#define THUMB_IMAGE_HEIGHT 70
#define PARAGRAPH_SPACING 3
#define HEADLINE_MAX_HEIGHT 18
#define SLUGLINE_MAX_HEIGHT 50

@implementation ArticleCell

@synthesize article = _article;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        thumbnailView = [[UILazyImageView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        thumbnailView.contentMode = UIViewContentModeScaleAspectFit;
        thumbnailView.layer.borderWidth = 1;
        thumbnailView.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        [self.contentView addSubview:thumbnailView];
        
        labelHeadline = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        labelHeadline.backgroundColor = [UIColor clearColor];
        labelHeadline.textColor = [UIColor colorWithWhite:0.2 alpha:1.0];
        labelHeadline.numberOfLines = 0;
        labelHeadline.font = [UIFont boldSystemFontOfSize:TITLE_FONE_SIZE];
        [self.contentView addSubview:labelHeadline];
        
        labelSlugLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
        labelSlugLine.backgroundColor = [UIColor clearColor];
        labelSlugLine.textColor = [UIColor colorWithWhite:116/255.0 alpha:1.0];
        labelSlugLine.numberOfLines = 0;
        labelSlugLine.font = [UIFont boldSystemFontOfSize:CONTENT_TEXT_FONT_SIZE];
        [self.contentView addSubview:labelSlugLine];
    }
    
    return self;
}

- (void)dealloc
{
    [labelHeadline release];
    [labelSlugLine release];
    
    [_article release];
    
    [super dealloc];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat nextY = CONTENT_MARGIN;
    
    // set frame for thumbnail image view
    thumbnailView.frame = CGRectMake(CONTENT_MARGIN, nextY, THUMB_IMAGE_WIDTH, THUMB_IMAGE_HEIGHT);
    
    CGFloat textWidth = self.contentView.width - CONTENT_MARGIN*2 - (thumbnailView.hidden ? 0 : thumbnailView.width);
    CGFloat textMargin = CONTENT_MARGIN + (thumbnailView.hidden ? 0 : thumbnailView.width + CONTENT_MARGIN);
    
    // set frame for label headline
    CGFloat headlineWidth = textWidth;
    CGSize headLineSize = [labelHeadline sizeThatFits:CGSizeMake(headlineWidth, CGFLOAT_MAX)];
    headLineSize.height = MIN(headLineSize.height, HEADLINE_MAX_HEIGHT);
    labelHeadline.frame = CGRectMake(textMargin, nextY, headlineWidth, headLineSize.height);
    nextY = labelHeadline.bottom + PARAGRAPH_SPACING;
    
    // set frame for label slugline
    CGFloat slugLineWidth = textWidth;
    CGSize slugLineSize = [labelSlugLine sizeThatFits:CGSizeMake(slugLineWidth, CGFLOAT_MAX)];
    slugLineSize.height = MIN(slugLineSize.height, SLUGLINE_MAX_HEIGHT);
    labelSlugLine.frame = CGRectMake(textMargin, nextY, slugLineWidth, slugLineSize.height);
    nextY = labelSlugLine.bottom + PARAGRAPH_SPACING;
}

- (void)setArticle:(Article *)article
{
    // make sure this methods is called on main thread
    assert([NSThread isMainThread]);
    
    if (article != _article) {
        [_article release], _article = [article retain];
        [self reloadData];
    }
}

- (void) reloadData
{
    thumbnailView.imageURL = self.article.thumbnailImageHref;
    thumbnailView.hidden = [NSString isNullOrEmpty:self.article.thumbnailImageHref];
    
    labelHeadline.text = self.article.headLine;
    labelSlugLine.text = self.article.slugLine;
    
    [self setNeedsLayout];
}

+ (CGFloat) tableView:(UITableView *)tableView rowHeightForArticle:(Article *)article accessoryViewWidth:(CGFloat)accessoryViewWidth
{
    CGFloat rowHeight = CONTENT_MARGIN * 2 + THUMB_IMAGE_HEIGHT;
    return rowHeight;
}

@end
